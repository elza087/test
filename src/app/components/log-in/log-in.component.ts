import {Component, OnDestroy, OnInit} from '@angular/core';
import {HeaderService} from "../header/header.service";
import {translations} from "../../translations/lng";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit, OnDestroy {
  public signForm: any;
  public translate: any;
  private translateSub: any;

  constructor(private headerSvc: HeaderService,
              private fb: FormBuilder) {
    this.translateSub = this.headerSvc.getLng().subscribe((lng: string) => {
      this.translate = translations[lng]
    });
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.translateSub.unsubscribe();
  }

  initForm() {
    this.signForm = this.fb.group({
      name: ['', Validators.required],
      userName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }

  get f() {
    return this.signForm.controls;
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (Object as any).values(formGroup.controls).forEach((control: any) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  onSubmit(event: any) {
    event.preventDefault();
    this.markFormGroupTouched(this.signForm);
    if (this.signForm.valid) {
      console.log(this.signForm.getRawValue());
    }
  }

}
