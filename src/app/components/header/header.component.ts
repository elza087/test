import { Component, OnInit } from '@angular/core';
import {HeaderService} from "./header.service";
import {translations} from "../../translations/lng";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public checkLng = localStorage.getItem('lng') ? localStorage.getItem('lng') : 'en';
  public translate: any;

  constructor(private headerSvc: HeaderService) {
    this.headerSvc.getLng().subscribe((lng: string) => {
      this.translate = translations[lng]
    });
  }

  ngOnInit(): void {
  }

  setLng(lng: string) {
    this.checkLng = lng;
    localStorage.setItem('lng', lng);
    this.headerSvc.setLng(lng);
  }

}
