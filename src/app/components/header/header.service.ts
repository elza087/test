import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable()
export class HeaderService {
  public lng = new BehaviorSubject<any>(localStorage.getItem('lng') ? localStorage.getItem('lng') : 'en');
  constructor() { }

  getLng() {
    return this.lng;
  }

  setLng(lng:string) {
    this.lng.next(lng);
  }
}
