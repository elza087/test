export const translations: any = {
  en: {
    page1: 'TEACHERS',
    page2: 'DONORS',
    page3: 'ABOUT US',
    page4: 'CONTACT US',
    page5: 'BLOG',
    signIn: 'Sign In',
    title: 'Sign Up',
    subTitle: 'It’s quick and easy.',
    forms: {
      name: 'Name',
      lastName: 'Last Name',
      email: 'Last Name',
      phone: 'Phone Number',
      password: 'Password',
      confirmPassword: 'Confirm Password',
    }
  },
  hy: {
    page1: 'ՈՒՍՈՒՑԻՉՆԵՐ',
    page2: 'ԴՈՆՈՐՆԵՐ',
    page3: 'ՄԵՐ ՄԱՍԻՆ',
    page4: 'ԿԱՊԵՔ ՄԵԶ',
    page5: 'ԲԼՈԳ',
    signIn: 'Մուտք գործել',
    title: 'Գրանցվել',
    subTitle: 'Դա արագ և հեշտ է:',
    forms: {
      name: 'Անուն',
      lastName: 'Ազգանուն',
      email: 'Էլ հասցեն',
      phone: 'Հեռախոսահամար',
      password: 'Գաղտնաբառ',
      confirmPassword: 'Հաստատել գաղտնաբառը',
    }
  },
  ru: {
    page1: 'УЧИТЕЛЯ',
    page2: 'ДОНОРЫ',
    page3: 'О НАС',
    page4: 'СВЯЗАТЬСЯ С НАМИ',
    page5: 'БЛОГ',
    signIn: 'Войти',
    title: 'Зарегистрироваться',
    subTitle: 'Это быстро и просто.',
    forms: {
      name: 'Имя',
      lastName: 'Фамилия',
      email: 'Эл. адрес',
      phone: 'Телефонный номер',
      password: 'Пароль',
      confirmPassword: 'Подтвердить Пароль',
    }
  }
};
